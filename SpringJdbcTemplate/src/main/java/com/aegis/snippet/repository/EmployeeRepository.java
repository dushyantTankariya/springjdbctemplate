package com.aegis.snippet.repository;

import com.aegis.snippet.bo.Employee;

import java.util.List;

/**
 * @author Dushyant Tankariya
 * @since 1.0.0.0
 * @see "Interface for transaction"
 * */
public interface EmployeeRepository {

    /**
     * @param employee {@link Employee}
     * */
    void insert(Employee employee);

    /**
     * @param employees list of {@link Employee}
     * */
    void insertBatch1(final List<Employee> employees);

    /**
     * @param sql insert based on query.
     * */
    void insertBatch2(final String sql);

    /**
     * @param id the integer value
     * @return {@link Employee}
     * */
    Employee findById(int id);

    /**
     * @param id to fetch one name column
     * @return {@link String}
     * */
    String findNameById(int id);

    /**
     * @return {@link List<Employee>}
     * */
    List<Employee> findAll();

    /**
     * @param id is to remove record from data tables.
     * */
    void deleteById(int id);

}
