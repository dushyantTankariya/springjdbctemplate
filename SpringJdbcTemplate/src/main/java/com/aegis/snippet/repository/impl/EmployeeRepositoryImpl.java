package com.aegis.snippet.repository.impl;

import com.aegis.snippet.bo.Employee;
import com.aegis.snippet.repository.EmployeeRepository;
import org.springframework.jdbc.core.BatchPreparedStatementSetter;
import org.springframework.jdbc.core.BeanPropertyRowMapper;
import org.springframework.jdbc.core.JdbcTemplate;

import javax.sql.DataSource;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

/**
 * @author Dushyant Tankariya
 * @since 1.0.0.0
 * @see "{@link EmployeeRepository}"
 * @see "Implementation of Employee Repository"
 * */
public class EmployeeRepositoryImpl implements EmployeeRepository {

    private DataSource dataSource;
    private JdbcTemplate jdbcTemplate;

    /**
     * @see "Default Constructor"
     * */
    public EmployeeRepositoryImpl(){}

    /**
     * @param dataSource | {@link DataSource}
     * @see "Setting up the data source for database transaction."
     * */
    public void setDataSource(DataSource dataSource){
        this.dataSource = dataSource;
    }

    /**
     * @param employee {@link Employee}
     * @see "To insert new Employee"
     * */
    @Override
    public void insert(Employee employee) {
        String sql = "INSERT INTO employee (ID, NAME, AGE) VALUES (?, ?, ?)";
        jdbcTemplate = new JdbcTemplate(dataSource,true);
        jdbcTemplate.update(sql,employee.employeeToObjectArrayFactory());
    }

    /**
     * @param employees list of {@link Employee}
     * @see "Insert records in batch"
     */
    @Override
    public void insertBatch1(List<Employee> employees) {
        jdbcTemplate = new JdbcTemplate(dataSource, true);
        String sql = "INSERT INTO EMPLOYEE (ID, NAME, AGE) VALUES (?, ?, ?)";
        jdbcTemplate.batchUpdate(sql, new BatchPreparedStatementSetter(){

            /**
             * @param ps the PreparedStatement to invoke setter methods on
             * @param i  index of the statement we're issuing in the batch, starting from 0
             * @throws {@link SQLException} if a SQLException is encountered
             *                      (i.e. there is no need to catch SQLException)
             * @see "Set parameter values on the given PreparedStatement."
             */
            @Override
            public void setValues(PreparedStatement ps, int i) throws SQLException {
                Employee employee = employees.get(i);
                ps.setLong(1, employee.getId());
                ps.setString(2, employee.getName());
                ps.setInt(3, employee.getAge() );
            }

            /**
             * @return the number of statements in the batch
             * @see "Return the size of the batch."
             */
            @Override
            public int getBatchSize() {
                return employees.size();
            }
        });
    }

    /**
     * @param sql batch insert based on query.
     */
    @Override
    public void insertBatch2(String sql) {
        jdbcTemplate = new JdbcTemplate(dataSource);
        jdbcTemplate.batchUpdate(new String[]{sql});
    }

    /**
     * @param id the integer value
     * @return {@link Employee}
     * @see "To find employee by id"
     * */
    @SuppressWarnings({ "unchecked", "rawtypes" })
    @Override
    public Employee findById(int id) {
        String sql = "SELECT * FROM EMPLOYEE WHERE ID = ?";
        jdbcTemplate = new JdbcTemplate(dataSource, true);

        /*Fetch one Employee by implementing RowMapper class*/
        //Employee employee = (Employee) jdbcTemplate.queryForObject(sql,new Object[] {id}, new EmployeeRowMapper());

        /*Fetch one Employee by BeanPropertyRowMapper*/
        return (Employee) jdbcTemplate.queryForObject(sql, new Object[]{id}, new BeanPropertyRowMapper(Employee.class));
    }

    /**
     * @param id to fetch one name column
     * @return {@link String}
     * @see "Find only employee name by id"
     */
    @Override
    public String findNameById(int id) {
        jdbcTemplate = new JdbcTemplate(dataSource, true);
        String sql = "SELECT NAME FROM EMPLOYEE WHERE ID = ?";
        return jdbcTemplate.queryForObject(sql, new Object[] {id}, String.class);

    }

    /**
     * @return {@link List<Employee>}
     * @see "To list all the employees"
     * */
    @Override
    public List<Employee> findAll() {
        jdbcTemplate = new JdbcTemplate(dataSource, true);
        String sql = "SELECT * FROM EMPLOYEE";
        List<Employee> employees = new ArrayList<>();
        List<Map<String, Object>> rows = jdbcTemplate.queryForList(sql);
        for (Map<String, Object> row: rows) {
            Employee employee = new Employee();
            employee.setId(Integer.parseInt(String.valueOf(row.get("ID"))));
            employee.setName(String.valueOf(row.get("NAME")));
            employee.setAge(Integer.parseInt(String.valueOf(row.get("AGE"))));
            employees.add(employee);
        }
        return employees;
    }

    /**
     * @param id is to remove record from data tables.
     * @see "To remove one record by employee id"
     */
    @Override
    public void deleteById(int id) {
        jdbcTemplate = new JdbcTemplate(dataSource, true);
        String sql = "DELETE FROM EMPLOYEE WHERE ID = ?";
        jdbcTemplate.update(sql,id);

    }
}