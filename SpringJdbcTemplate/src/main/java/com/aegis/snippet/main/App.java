package com.aegis.snippet.main;

import com.aegis.snippet.bo.Employee;
import com.aegis.snippet.repository.EmployeeRepository;
import org.springframework.context.ConfigurableApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

/**
 * @author Dushyant Tankariya
 * @since 1.0.0.0
 * @see "Main Class where Program starts execution"
 * */
public class App {

    public static Integer ID = 1;

    /**
     * @see "Default constructor"
     * */
    App(){}

    /**
     * @param args | {@link String} |
     * @see "Program execution starts from here"
     * */
    public static void main(String[] args){

        //Load Bean configuration
        ConfigurableApplicationContext context = new ClassPathXmlApplicationContext(    "applicationContext.xml");
        EmployeeRepository employeeRepository = (EmployeeRepository) context.getBean("employeeRepository");

        Employee employee;
        List<Employee> employees;
        /*Insert Employee to the database*/
        employee = new Employee(App.ID,"Adam Smith",23);
        System.out.println("Insert one row: " + employee);
        employeeRepository.insert(employee);

        /* Batch Insert 1
        2	Kana Banner	27
        3   Paul walker 61
        */
        employees = new ArrayList<>();
        employee = new Employee(2, "Kana Banner", 27);
        employees.add(employee);
        employee = new Employee(3, "Paul Walker", 61);
        employees.add(employee);
        employeeRepository.insertBatch1(employees);
        System.out.println("Batch Insert 1: " + employeeRepository.findAll());

        /* Batch Insert 2
        4   James wesley    61
        */
        employeeRepository.insertBatch2("INSERT INTO EMPLOYEE(ID, NAME, AGE) VALUES(4, 'James wesley', 61)");
        System.out.println("Batch Insert 2: " + employeeRepository.findAll());

        /*Find one employee from id*/
        employee = employeeRepository.findById(App.ID);
        System.out.println("Find one " + employee + " by id [" + App.ID + "]");

        /*Find Name By employee Id*/
        String employeeName = employeeRepository.findNameById(App.ID);
        System.out.println("Find Name " + employeeName + " by Employee Id " + App.ID );

        /*Find all employee*/
        System.out.println("Find All: " + employeeRepository.findAll());

        /* Delete one record by employee Id*/
        employeeRepository.deleteById(App.ID);
        System.out.println("Removed first record: " + employeeRepository.findAll());

        //Close Bean context
        context.close();
    }
}
