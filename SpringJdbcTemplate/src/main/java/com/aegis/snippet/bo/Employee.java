package com.aegis.snippet.bo;

import java.io.Serializable;

/**
 * @author Dushyant Tankariya
 * @since 1.0.0.0
 * @see "Employee BO / Model (Business Object / Model)"
 */
public class Employee implements Serializable {

    private Integer id;
    private String name;
    private Integer age;

    public Employee() { }

    public Employee(Integer id, String name, Integer age) {
        this.id = id;
        this.name = name;
        this.age = age;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Integer getAge() {
        return age;
    }

    public void setAge(Integer age) {
        this.age = age;
    }

    public Object[] employeeToObjectArrayFactory(){
        return new Object[] {id, name, age};
    }

    @Override
    public String toString() {
        return "Employee{" + "id=" + id + ", name='" + name + '\'' + ", age=" + age + '}';
    }
}
