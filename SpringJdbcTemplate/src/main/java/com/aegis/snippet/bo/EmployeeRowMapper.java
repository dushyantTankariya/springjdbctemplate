package com.aegis.snippet.bo;

import org.springframework.jdbc.core.RowMapper;

import java.sql.ResultSet;
import java.sql.SQLException;

/**
 * @author Dushyant Tankariya
 * @since 1.0.1.0
 * @see "To map the row with Bussiness Object properties"
 * @see "Implmeents {@link RowMapper}"
 * */
public class EmployeeRowMapper implements RowMapper {

    /**
     * @param resultSet fetched from database using repository layer
     * @param rowNum if returned with multiple rows
     * @return Object of retrieved data.
     * */
    @Override
    public Object mapRow(ResultSet resultSet, int rowNum) throws SQLException {
        Employee employee = new Employee();
        employee.setId(resultSet.getInt("ID"));
        employee.setName(resultSet.getString("NAME"));
        employee.setAge(resultSet.getInt("AGE"));
        return employee;
    }
}
